![pokemon](https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/International_Pok%C3%A9mon_logo.svg/300px-International_Pok%C3%A9mon_logo.svg.png)

# **Teste Front-End ReactJS**

## 💻 **O desafio**

Desenvolver uma aplicação web com o objetivo de avaliarmos o seu domínio em front-end, ou seja, sua organização, estilo e boas práticas com o código, conhecimento dos frameworks e tecnologias utilizadas.

#
## 🔗 **Links**
#### **Segue links uteis para resolução do desafio:**
- [PokéApi](https://pokeapi.co/)
- [Layout Desktop](https://www.figma.com/proto/l92meWj5EzwY3q8XZro1i0/Teste-Front?node-id=13%3A13571&scaling=min-zoom)
- [Layout Mobile](https://www.figma.com/proto/l92meWj5EzwY3q8XZro1i0/Teste-Front?node-id=41%3A18782&scaling=min-zoom)

#
## 📝 **Fluxo e Funcionalidades da aplicação**
- [ ] Home page com um botão para iniciar a aplicação (qualquer url inexistente deve redirecionar o usuário para essa home page).
- [ ] Personagem no centro da página.
- [ ] Barra na esquerda indicando quantos Pokémons ele já capturou (limite de 6) + botão de criação.
- [ ] Ao passar o mouse no personagem é exibido o tooltip correspondente.
- [ ] Ao clicar no personagem é feita uma busca por um Pokémon aleatório (id de 1 a 807).
- [ ] Com o resultado da busca é aberto um modal mostrando os detalhes do Pokémon.
- [ ] Usuário tem a opção de capturá-lo, clicando na pokébola, ou fechar o modal.
- [ ] Se ele capturar o Pokémon, esse Pokémon é exibido na SideBar e o modal de captura desaparece.
- [ ] Usuário pode capturar até 6 Pokémons.
- [ ] Selecionando qualquer Pokémon na SideBar o usuário pode ver os detalhes do Pokémon.
- [ ] O(s) tipo(s) do Pokémon deve ser traduzido (ex: water => Água).
- [ ] Usuário pode editar SOMENTE o nome de um Pokémon que foi capturado.
- [ ] Na SideBar o usuário tem a possibilidade de criar um Pokémon (um Pokémon pode ter no máximo 2 "tipos").
- [ ] O formulário de criação de Pokémon deve conter validações em todos os campos.
- [ ] Caso algum campo não esteja preenchido, o botão de criação deve ficar bloqueado.
- [ ] Para um Pokémon criado o usuário pode editar qualquer informação ou liberá-lo.
- [ ] Sempre que liberar um Pokémon é possível capturar outro através da busca ou criar um customizado.
- [ ] Caso as 6 posições estejam ocupadas o usuário não pode mais buscar nem criar novos Pokémons.
- [ ] Responsividade para resoluções desktop e mobile. (Ex: 1280 x 720, 360 x 740)

Obs: Marque com um "x" as funcionalidades que foram realizadas
#

## 🚀 **Tecnologias** 
#### **Para a resolução do desafio, esperamos que você faça uso das tecnologias citadas abaixo:** 
- react
- hooks
- redux (apesar de ser uma aplicação pequena, queremos poder avaliar seus conhecimentos com a tecnologia)
- react-redux
- redux-sagas
- redux-thunk
- styled-components
- prop-types
- formik
- yup

## 📖 **Regras**
- Todo o seu código deve ser disponibilizado em um repositório público;
- Envie o link para do repositorio para o mesmo email em que o teste foi enviado
- Coloque no assunto: Teste_Front-End_ReactJS - [Seu nome];
- Utilizar a [PokéApi](https://pokeapi.co/) para interagir com a aplicação;
- Usar componentes funcionais e hooks;

#
## ➕ **Opcional**
#### **Esperamos que você tenha curiosidade em criar testes para aplicação, mas não se preocupe, isso é opcional** 
 - Teste unitário
 - Teste de integração
 - Teste de interface
#

