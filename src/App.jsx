import React from "react";

import GlobalStyles from "assets/styles/globalStyles";

import Routes from "./routes";

function App() {
  return (
    <>
      <GlobalStyles />
      <Routes />
    </>
  );
}

export default App;
