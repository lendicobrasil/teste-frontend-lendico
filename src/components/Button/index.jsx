import React from "react";

import * as S from "./styles";

const Button = ({ text, icon, onClick }) => (
  <S.Wrapper className={`${icon ? "icon" : ""}`} onClick={onClick}>
    {icon ? <S.Icon src={icon} /> : <S.Text>{text}</S.Text>}
  </S.Wrapper>
);

export default Button;
