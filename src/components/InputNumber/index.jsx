import React from "react";

import chevron from "assets/images/chevronDownBlack.png";

import * as S from "./styles";

const InputNumber = ({ className, label, placeholder, name, suffix }) => (
  <S.Wrapper className={className}>
    {label && <S.Label>{label}</S.Label>}

    <S.Content>
      <S.Input value="" type="number" placeholder={placeholder} name={name} />

      {suffix && <S.Suffix>{suffix}</S.Suffix>}

      <S.Actions>
        <S.Arrow src={chevron} className="increase" alt="Mais" />
        <S.Arrow src={chevron} className="decrease" alt="Menos" />
      </S.Actions>
    </S.Content>
  </S.Wrapper>
);

export default InputNumber;
