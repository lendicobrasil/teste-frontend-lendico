import React from "react";

import * as S from "./styles";

const InputText = ({ className, label, type, placeholder, name }) => (
  <S.Wrapper className={className}>
    {label && <S.Label>{label}</S.Label>}

    <S.Input type={type} placeholder={placeholder} name={name} />
  </S.Wrapper>
);

export default InputText;
