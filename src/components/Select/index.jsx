import React from "react";

import chevron from "assets/images/chevronDownBlack.png";

import * as S from "./styles";

const Select = ({ label, options }) => (
  <S.Wrapper>
    {label && <S.Label>{label}</S.Label>}

    <S.Content>
      <S.Select>
        <S.Option value="">Selecione o(s) tipo(s)</S.Option>
        {options &&
          options.map((option, index) => (
            <S.Option key={index} value={option.value}>
              {option.text}
            </S.Option>
          ))}
      </S.Select>
      <S.Icon src={chevron} alt="Chevron" />
    </S.Content>
  </S.Wrapper>
);

export default Select;
