import React from "react";

import Button from "components/Button";

import iconPlus from "assets/images/plus.png";

import * as S from "./styles";

const Sidebar = () => (
  <S.Wrapper>
    <S.List>
      <S.Item>?</S.Item>
    </S.List>

    <Button icon={iconPlus} />
  </S.Wrapper>
);

export default Sidebar;
