import React from "react";

import Sidebar from "components/Sidebar";

import * as S from "./styles";

const MapPage = () => (
  <S.Wrapper>
    <Sidebar />

  </S.Wrapper>
);

export default MapPage;
